package facade

type MqttConfig struct {
	Url      string
	Username string
	Password string
}

type MqttClient interface {
	Disconnect()
	// Client Id for identified who's connect to message queue
	ClientId() string
	// Publish message to queue with topic. If messageId is blank. The system will auto generated.
	Publish(topic string, messageId string, qos byte, retained bool, payload interface{}) string
	Subscribe(topic string, qos byte, callback func(topic string, msgPayload string))
	Unsubscribe(topic string) error
}
