package facade

type Kafka interface {
	Debug(debug bool)
	Publish(topic string, groupId string, message []byte)
}
