package facade

type Base struct {
	Id                 string        `structs:"id" json:"id" bson:"id"`
	Ids                []interface{} `structs:"ids" json:"ids" bson:"ids"`
	ItemId             string        `structs:"itd" json:"itd" bson:"itd"`
	BeginDate          string        `structs:"bd" json:"bd" bson:"bd"`
	EndDate            string        `structs:"ed" json:"ed" bson:"ed"`
	IndexName          string        `structs:"idx" json:"idx" bson:"idx"`
	RecId              int64         `structs:"rid" json:"rid" bson:"rid"`
	SessionKey         string
	TokenId            string
	Database           string
	Collection         string
	Username           string
	Uid                int64
	UserTypeCode       string
	AsyncUniqueId      string
	DocType            string
	ProfilePartnerCode string
	ProfileUserCode    string
	CompanyCode        string `structs:"b1" json:"b1" bson:"b3"`
	CompanyName        string `structs:"b2" json:"b2" bson:"b2"`
	CompanyId          int    `structs:"b3" json:"b3" bson:"b3"`
	From               int    `structs:"b4" json:"b4" bson:"b4"`
	Size               int    `structs:"b5" json:"b5" bson:"b5"`
	Query              string `structs:"b6" json:"b6" bson:"b6"`
	TerminateAfterSize int    `structs:"b7" json:"b7" bson:"b7"`
	Offset             int    `structs:"b8" json:"b8" bson:"b8"`
	Limit              int    `structs:"b9" json:"b9" bson:"b9"`
	CompanyAddress     int    `structs:"b10" json:"b10" bson:"b10"`
	SortMap            map[string]string
	DataModel          interface{}
}

type RequestBase struct {
	Data interface{} `structs:"data" json:"data" bson:"data"`
}
