package facade

type MediationContext interface {
	Logger() Logger
	TraceContext() TraceContext
	// For new trace context.
	SetTraceContext(traceCtx TraceContext)
	Data() interface{}
}
