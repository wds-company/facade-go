package facade

type Adapter interface {
	SetCsv(CsvHandler)
	Csv() CsvHandler
	SetElastic(ElasticHandler)
	Elastic() ElasticHandler
	SetNoSql(CRUDDocumentStore)
	NoSql() CRUDDocumentStore
	SetOrm(OrmHandler)
	Orm() OrmHandler
	SetKafka(Kafka)
	Kafka() Kafka
}
