// Package facade provides provide common libraries.
package facade

import "reflect"

type Config struct {
	Overwrite                    bool
	AppendSlice                  bool
	TypeCheck                    bool
	Transformers                 Transformers
	OverwriteWithEmptyValue      bool
	OverwriteSliceWithEmptyValue bool
}

type Transformers interface {
	Transformer(reflect.Type) func(dst, src reflect.Value) error
}

type StructMerge interface {

	// Merge will fill any empty for value type attributes on the dst struct using corresponding
	// src attributes if they themselves are not empty. dst and src must be valid same-type structs
	// and dst must be a pointer to struct.
	// It won't merge unexported (private) fields and will do recursively any exported field.
	Merge(dst, src interface{}, opts ...func(*Config)) error

	// WithTransformers adds transformers to merge, allowing to customize the merging of some types.
	WithTransformers(transformers Transformers) func(*Config)

	// WithOverride will make merge override non-empty dst attributes with non-empty src attributes values.
	WithOverride(config *Config)

	// WithOverwriteWithEmptyValue will make merge override non empty dst attributes with empty src attributes values.
	WithOverwriteWithEmptyValue(config *Config)

	// WithOverrideEmptySlice will make merge override empty dst slice with empty src slice.
	WithOverrideEmptySlice(config *Config)

	// WithAppendSlice will make merge append slices instead of overwriting it.
	WithAppendSlice(config *Config)

	// WithTypeCheck will make merge check types while overwriting it (must be used with WithOverride).
	WithTypeCheck(config *Config)
}
