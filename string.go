package facade

import "time"

type StringHandler interface {
	// Trim string
	Trim(s string, cutSet ...string) string

	// TrimLeft char in the string.
	TrimLeft(s string, cutSet ...string) string

	// TrimRight char in the string.
	TrimRight(s string, cutSet ...string) string

	// FilterEmail filter email, clear invalid chars.
	FilterEmail(s string) string

	// Substr for a string.
	Substr(s string, pos, length int) string

	// Padding a string.
	Padding(s, pad string, length int, pos uint8) string

	// PadLeft a string.
	PadLeft(s, pad string, length int) string

	// PadRight a string.
	PadRight(s, pad string, length int) string

	// Repeat repeat a string
	Repeat(s string, times int) string

	// RepeatRune repeat a rune char.
	RepeatRune(char rune, times int) (chars []rune)

	// BoolValue parse string to bool
	Bool(s string, defaultValue bool) bool

	// StringValue parse string to string
	String(s string, defaultValue string) string

	// FloatValue parse string to float32
	Float(s string, defaultValue float32) float32

	// FloatValue parse string to float64
	Float64(s string, defaultValue float64) float64

	// IntegerValue parse string to int64
	Int64(s string, defaultValue int64) int64

	// IntegerValue parse string to int
	Int(s string, defaultValue int) int

	// Array alias of the ToSlice()
	Array(s string, sep ...string) []string

	// Slice split string to array.
	Slice(s string, sep ...string) []string

	// Time convert date string to time.Time
	Time(s string, layouts ...string) (t time.Time, err error)

	// RandomBytes generate
	RandomBytes(length int) ([]byte, error)

	// RandomString generate.
	// Example:
	// this will give us a 44 byte, base64 encoded output
	// 	token, err := RandomString(32)
	// 	if err != nil {
	//     // Serve an appropriately vague error to the
	//     // user, but log the details internally.
	// 	}
	RandomString(length int) (string, error)

	// Gernate random string base on length.
	SecureRandomString(length int) string

	// Check is exist data in array
	InArray(v interface{}, in interface{}) (ok bool, i int)

	// Validate empty string.
	IsEmptyString(s string) bool

	// Validate not empty string.
	IsNotEmptyString(s string) bool

	// Get byte array of data.
	GetByteArray(s string) []byte

	// Check string data in array.
	InArrayString(val string, array []string) (ok bool, i int)

	// Equal reports whether s and t, interpreted as UTF-8 strings, are equal under Unicode ignore case.
	EqualIgoreCase(s, t string) bool

	// Generate sortable unique identifier
	GenerateSortableUid() string

	// ReplaceAll returns a copy of the string s with all non-overlapping instances of old replaced by new. If old is empty, it matches at the beginning of the string and after each UTF-8 sequence, yielding up to k+1 replacements for a k-rune string.
	ReplaceAll(s, old, new string) string

	// Split slices s into all substrings separated by sep and returns a slice of the substrings between those separators.
	// If s does not contain sep and sep is not empty, Split returns a slice of length 1 whose only element is s.
	// If sep is empty, Split splits after each UTF-8 sequence. If both s and sep are empty, Split returns an empty slice.
	// It is equivalent to SplitN with a count of -1.
	Split(s, sep string) []string

	// Lower returns a copy of the string s with all Unicode letters mapped to their lower case.
	Lower(s string) string

	// Uppercase alias of the strings.ToUpper()
	Upper(s string) string

	// UpperWord Change the first character of each word to uppercase
	UpperWord(s string) string

	// LowerFirst lower first char
	LowerFirst(s string) string

	// UpperFirst upper first char
	UpperFirst(s string) string

	// Uppercase for first charater of every wordss.
	UpperTitle(s string) string

	// Match string from regular expression.
	Match(s string, pattern string) bool

	// Validate string from range of length (min -> max).
	IsStrLenValid(s string, minLength int, maxLength int) bool

	// Generate UUID in the RFC 2141 form, Ex. urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
	GenerateUuid(urn bool) string

	// Snake alias of the SnakeCase
	Snake(s string, sep ...string) string

	// SnakeCase convert. eg "RangePrice" -> "range_price"
	SnakeCase(s string, sep ...string) string

	// Camel alias of the CamelCase
	Camel(s string, sep ...string) string

	// CamelCase convert string to camel case.
	// Support:
	// 	"range_price" -> "rangePrice"
	// 	"range price" -> "rangePrice"
	// 	"range-price" -> "rangePrice"
	CamelCase(s string, sep ...string) string

	// Md5 Generate a 32-bit md5 string
	Md5(src interface{}) string

	// GenMd5 Generate a 32-bit md5 string
	GenMd5(src interface{}) string

	// Base64 base64 encode
	Base64(str string) string

	// B64Encode base64 encode
	B64Encode(str string) string

	// URLEncode encode url string.
	URLEncode(s string) string

	// URLDecode decode url string.
	URLDecode(s string) string
}
