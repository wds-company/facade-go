package facade

type CsvHandler interface {
	Debug(debug bool)
	OpenFile(csvInputFile string) error
	Read() ([]string, error)
	ReadModel(model interface{}) (interface{}, error)
}
